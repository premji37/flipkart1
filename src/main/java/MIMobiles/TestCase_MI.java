package MIMobiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestCase_MI {

	@Test
	//public static void main(String[] args) throws InterruptedException {
	public void flipkart() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		//driver.findElementByXPath("//button[text()='✕']").click();
		WebElement Electronic = driver.findElementByXPath("//span[text()='Electronics']");
		Actions mou = new Actions(driver);
		mou.moveToElement(Electronic).perform();
		WebElement MI = driver.findElementByXPath("//a[text()='Mi']");
		Thread.sleep(500);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(MI));
		MI.click();
		WebDriverWait titlewait = new WebDriverWait(driver, 20);
		titlewait.until(ExpectedConditions.titleContains("Mi Mobile Phones"));
        String title = driver.getTitle();
        System.out.println(title);
        if (title.contains("Mi Mobile Phones"))
	    System.out.println("The Title is correct");
        else
        System.out.println("The title is not correct");
        driver.findElementByXPath("//div[text()='Newest First']").click();
       /* List<WebElement> products = driver.findElementsByXPath("//div[@class='_3wU53n']");
        WebDriverWait prods = new WebDriverWait(driver, 30);
        prods.until(ExpectedConditions.visibilityOfAllElements(products));
        for (WebElement Elements : products) {
        	      	System.out.println(Elements.getText());   
        }
        List<WebElement> Prizes = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
        WebDriverWait priz = new WebDriverWait(driver, 30);
        priz.until(ExpectedConditions.visibilityOfAllElements(Prizes));
        for (WebElement prize : Prizes) {
              	System.out.println(prize.getText()); 
			}*/ //Like this way
        Thread.sleep(500);
        List<WebElement> products = driver.findElementsByXPath("//div[@class='_3wU53n']");
        List<WebElement> Prizes = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
        WebDriverWait prods = new WebDriverWait(driver, 30);
        prods.until(ExpectedConditions.visibilityOfAllElements(products));
        for (int i=0 ;i<products.size();i++) {
        	System.out.println(products.get(i).getText() +":"+ Prizes.get(i).getText());
        	
        }
        WebElement firele = driver.findElementByXPath("//div[@class='_3wU53n']");
       firele.click();
        wait.until(ExpectedConditions.visibilityOfAllElements(firele));
        Set<String> windowHandles = driver.getWindowHandles();
		List<String> lst=new ArrayList<>();
		lst.addAll(windowHandles);
		driver.switchTo().window(lst.get(1));
		WebElement First = driver.findElementByXPath("//span[@class='_35KyD6']");
		WebDriverWait nextwin = new WebDriverWait(driver, 30);
		nextwin.until(ExpectedConditions.visibilityOfAllElements(First));
        String Firsttext = First.getText();
        System.out.println("text"+Firsttext);
        String pagetitle = driver.getTitle();
        System.out.println("title"+pagetitle);
        if(pagetitle.contains("Redmi 6 Pro"))
        System.out.println("The title is correct");
        else
        System.out.println("the title is not correct");
        WebElement Rat = driver.findElementByXPath("//span[@class='_38sUEc']//span");
        System.out.println(Rat.getText());
       /* WebElement Rev = driver.findElementByXPath("//span[contains(text(),'Reviews')");
        System.out.println("Ratings"+Rev.getText());*/
        
        driver.quit();
        
}
}